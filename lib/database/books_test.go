package database

import "testing"

var book = Book{
	ID:      "r_m-IOzzIbA6QK5w",
	Title:   "some title",
	Authors: []string{"Alice", "Bob"},
}

func TestAddAndDeleteBook(t *testing.T) {
	db, dbclose := testDbInit(t)
	defer dbclose()

	testAddBook(t, db)

	books, num, err := db.GetNewBooks("", 1, 0)
	if err != nil {
		t.Fatal("db.GetNewBooks() return an error: ", err)
	}
	if num < 1 {
		t.Fatalf("db.GetNewBooks() didn't find any result.")
	}
	if len(books) < 1 {
		t.Fatalf("db.GetNewBooks() didn't return any result.")
	}
	if books[0].Title != book.Title {
		t.Error("Book title don't match : '", books[0].Title, "' <=> '", book.Title, "'")
	}

	err = db.DeleteBook(books[0].ID)
	if err != nil {
		t.Fatal("db.DeleteBook() return an error: ", err)
	}
	books, num, err = db.GetNewBooks("", 1, 0)
	if err != nil {
		t.Fatal("db.GetNewBooks() return an error after delete: ", err)
	}
	if num != 0 {
		t.Fatalf("the book was not deleted.")
	}
}

func TestActiveBook(t *testing.T) {
	db, dbclose := testDbInit(t)
	defer dbclose()

	testAddBook(t, db)
	books, _, _ := db.GetNewBooks("", 1, 0)
	id := books[0].ID

	err := db.ActiveBook(id)
	if err != nil {
		t.Fatal("db.ActiveBook(", id, ") return an error: ", err)
	}

	b, err := db.GetBookID(id)
	if err != nil {
		t.Fatal("db.GetBookID(", id, ") return an error: ", err)
	}
	if !b.Active {
		t.Error("Book is not activated")
	}
	if b.Authors[0] != books[0].Authors[0] {
		t.Error("Book author don't match : '", b.Authors, "' <=> '", book.Authors, "'")
	}

	bs, num, err := db.GetBooks(book.Title, 20, 0)
	if err != nil {
		t.Fatal("db.GetBooks(", book.Title, ") return an error: ", err)
	}
	if num != 1 || len(bs) != 1 {
		t.Fatal("We got a un expected number of books: ", num, bs)
	}
	if bs[0].Authors[0] != book.Authors[0] {
		t.Error("Book author don't match : '", bs[0].Authors, "' <=> '", book.Authors, "'")
	}

	bs, num, err = db.GetBooks("none", 20, 0)
	if err != nil {
		t.Fatal("db.GetBooks(none) return an error: ", err)
	}
	if num != 0 || len(bs) != 0 {
		t.Error("We got books: ", num, bs)
	}
}

func TestUpdateBook(t *testing.T) {
	db, dbclose := testDbInit(t)
	defer dbclose()

	testAddBook(t, db)

	newTitle := "other title"
	err := db.UpdateBook(book.ID, map[string]interface{}{
		"title": newTitle,
	})
	if err != nil {
		t.Fatal("db.UpdateBook() return an error: ", err)
	}

	books, num, err := db.GetNewBooks("", 1, 0)
	if err != nil || num != 1 || len(books) != 1 {
		t.Fatal("db.GetNewBooks() return an error: ", err)
	}
	if books[0].Title != newTitle {
		t.Error("Book title don't match : '", books[0].Title, "' <=> '", newTitle, "'")
	}
}

func testAddBook(t *testing.T, db DB) {
	err := db.AddBook(book)
	if err != nil {
		t.Error("db.AddBook(", book, ") return an error:", err)
	}
}
