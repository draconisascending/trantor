package database

import "testing"

func TestNews(t *testing.T) {
	const text = "Some news text"

	db, dbclose := testDbInit(t)
	defer dbclose()

	err := db.AddNews(text)
	if err != nil {
		t.Errorf("db.News(", text, ") return an error: ", err)
	}

	news, err := db.GetNews(1, 1)
	if err != nil {
		t.Fatalf("db.GetNews() return an error: ", err)
	}
	if len(news) < 1 {
		t.Fatalf("No news found.")
	}
	if news[0].Text != text {
		t.Errorf("News text don't match : '", news[0].Text, "' <=> '", text, "'")
	}
}

func TestTwoNews(t *testing.T) {
	const text = "Some news text"
	const text2 = "More news"

	db, dbclose := testDbInit(t)
	defer dbclose()

	err := db.AddNews(text)
	if err != nil {
		t.Errorf("db.News(", text, ") return an error: ", err)
	}

	err = db.AddNews(text2)
	if err != nil {
		t.Errorf("db.News(", text, ") return an error: ", err)
	}

	news, err := db.GetNews(2, 1)
	if err != nil {
		t.Fatalf("db.GetNews() return an error: ", err)
	}
	if len(news) < 2 {
		t.Fatalf("No news found.")
	}
}
