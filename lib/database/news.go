package database

import (
	"fmt"
	"time"
)

// New entry in the news table
type New struct {
	ID   int
	Date time.Time
	Text string
}

// AddNews creates a new entry
func (db *pgDB) AddNews(text string) error {
	return db.AddRawNews(text, time.Now())
}

func (db *pgDB) AddRawNews(text string, date time.Time) error {
	return db.sql.Insert(&New{
		Text: text,
		Date: date,
	})
}

// GetNews returns all the news for the last 'days' limiting with a maximum of 'num' results
func (db *pgDB) GetNews(num int, days int) ([]New, error) {
	var news []New
	query := db.sql.Model(&news)

	if days != 0 {
		daystr := fmt.Sprintf("%d days", days)
		query = query.Where("date > CURRENT_TIMESTAMP-?::interval", daystr)
	}

	err := query.Limit(num).
		Order("date DESC").
		Select()
	return news, err
}
