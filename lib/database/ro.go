package database

import (
	"errors"
	"time"
)

type roDB struct {
	db DB
}

func (db *roDB) Close() error {
	return db.db.Close()
}

func (db *roDB) AddBook(book Book) error {
	return errors.New("RO database")
}

func (db *roDB) GetBooks(query string, length int, start int) (books []Book, num int, err error) {
	return db.db.GetBooks(query, length, start)
}

func (db *roDB) GetNewBooks(query string, length int, start int) (books []Book, num int, err error) {
	return db.db.GetNewBooks(query, length, start)
}

func (db *roDB) GetBookID(id string) (Book, error) {
	return db.db.GetBookID(id)
}

func (db *roDB) DeleteBook(id string) error {
	return errors.New("RO database")
}

func (db *roDB) UpdateBook(id string, data map[string]interface{}) error {
	return errors.New("RO database")
}

func (db *roDB) ActiveBook(id string) error {
	return errors.New("RO database")
}

func (db *roDB) IsBookActive(id string) bool {
	return db.db.IsBookActive(id)
}

func (db *roDB) AddUser(name string, pass string) error {
	return errors.New("RO database")
}

func (db *roDB) AddRawUser(name string, hpass []byte, salt []byte, role string) error {
	return errors.New("RO database")
}

func (db *roDB) GetRole(name string) (string, error) {
	return db.db.GetRole(name)
}

func (db *roDB) ValidPassword(name string, pass string) bool {
	return db.db.ValidPassword(name, pass)
}

func (db *roDB) SetPassword(name string, pass string) error {
	return errors.New("RO database")
}

func (db *roDB) AddNews(text string) error {
	return errors.New("RO database")
}

func (db *roDB) AddRawNews(text string, date time.Time) error {
	return errors.New("RO database")
}

func (db *roDB) GetNews(num int, days int) (news []New, err error) {
	return db.db.GetNews(num, days)
}

func (db *roDB) IncViews(ID string) error {
	return nil
}

func (db *roDB) IncDownloads(ID string) error {
	return nil
}

func (db *roDB) GetFrontPage() FrontPage {
	return db.db.GetFrontPage()
}
