package trantor

import (
	log "github.com/cihub/seelog"

	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/trantor/trantor/lib/database"
	"gitlab.com/trantor/trantor/lib/parser"
)

const (
	newItemsPage = 10
)

func deleteHandler(h handler) {
	if !h.sess.IsModerator() {
		notFound(h)
		return
	}

	var titles []string
	var isNew bool
	ids := strings.Split(mux.Vars(h.r)["ids"], "/")
	for _, id := range ids {
		if id == "" {
			continue
		}
		book, err := h.db.GetBookID(id)
		if err != nil {
			h.sess.Notify("Book not found!", "The book with id '"+id+"' is not there", "error")
			continue
		}
		h.store.Delete(id)
		h.db.DeleteBook(id)

		if !book.Active {
			isNew = true
		}
		titles = append(titles, book.Title)
	}
	if titles != nil {
		h.sess.Notify("Removed books!", "The books "+strings.Join(titles, ", ")+" are completly removed", "success")
	}
	h.sess.Save(h.w, h.r)
	if isNew {
		http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
	} else {
		http.Redirect(h.w, h.r, "/", http.StatusFound)
	}
}

func editHandler(h handler) {
	id := mux.Vars(h.r)["id"]
	if !h.sess.IsModerator() {
		notFound(h)
		return
	}
	book, err := h.db.GetBookID(id)
	if err != nil {
		notFound(h)
		return
	}

	var data bookData
	data.Book = book
	data.S = GetStatus(h)
	author := ""
	if len(book.Authors) > 0 {
		author = " by " + book.Authors[0]
	}
	data.S.Title = book.Title + author + " -- Edit -- " + data.S.Title
	h.load("edit", data)
}

func cleanEmptyStr(s []string) []string {
	var res []string
	for _, v := range s {
		if v != "" {
			res = append(res, v)
		}
	}
	return res
}

func saveHandler(h handler) {
	// XXX: check for errors (ISBN, length(lang), ...)
	id := mux.Vars(h.r)["id"]
	if !h.sess.IsModerator() {
		notFound(h)
		return
	}

	title := h.r.FormValue("title")
	publisher := h.r.FormValue("publisher")
	date := h.r.FormValue("date")
	description := h.r.FormValue("description")
	authors := cleanEmptyStr(h.r.Form["author"])
	tags := cleanEmptyStr(strings.Split(h.r.FormValue("tags"), ","))
	isbn := parser.ISBN(h.r.FormValue("isbn"))
	lang := h.r.FormValue("lang")
	book := map[string]interface{}{"title": title,
		"publisher":   publisher,
		"date":        date,
		"description": description,
		"authors":     authors,
		"tags":        tags,
		"isbn":        isbn,
		"lang":        lang}
	err := h.db.UpdateBook(id, book)
	if err != nil {
		log.Error("Updating book: ", err)
		h.sess.Notify("Can't modify book!", err.Error(), "error")
	} else {
		h.sess.Notify("Book Modified!", "", "success")
	}

	h.sess.Save(h.w, h.r)
	if h.db.IsBookActive(id) {
		http.Redirect(h.w, h.r, "/book/"+id, http.StatusFound)
	} else {
		// XXX: I can't use a referer here :(
		http.Redirect(h.w, h.r, "/new/", http.StatusFound)
	}
}

type newBook struct {
	TitleFound  int
	AuthorFound int
	B           database.Book
}
type newData struct {
	S      Status
	Found  int
	Books  []newBook
	Page   int
	Next   string
	Prev   string
	Search string
}

func newHandler(h handler) {
	if !h.sess.IsModerator() {
		notFound(h)
		return
	}

	err := h.r.ParseForm()
	if err != nil {
		http.Error(h.w, err.Error(), http.StatusInternalServerError)
		return
	}
	req := strings.Join(h.r.Form["q"], " ")
	page := 0
	if len(h.r.Form["p"]) != 0 {
		page, err = strconv.Atoi(h.r.Form["p"][0])
		if err != nil {
			page = 0
		}
	}
	res, num, _ := h.db.GetNewBooks(req, newItemsPage, page*newItemsPage)

	var data newData
	data.S = GetStatus(h)
	data.S.Title = "New books -- " + data.S.Title
	data.Found = num
	if num-newItemsPage*page < newItemsPage {
		data.Books = make([]newBook, num-newItemsPage*page)
	} else {
		data.Books = make([]newBook, newItemsPage)
	}
	for i, b := range res {
		data.Books[i].B = b
		_, data.Books[i].TitleFound, _ = h.db.GetBooks("title:"+b.Title, 1, 0)
		_, data.Books[i].AuthorFound, _ = h.db.GetBooks("author:"+strings.Join(b.Authors, " author:"), 1, 0)
	}
	data.Page = page + 1
	if num > (page+1)*newItemsPage {
		data.Next = "/new/?q=" + req + "&p=" + strconv.Itoa(page+1)
	}
	if page > 0 {
		data.Prev = "/new/?q=" + req + "&p=" + strconv.Itoa(page-1)
	}
	data.Search = req
	h.load("new", data)
}

func storeHandler(h handler) {
	if !h.sess.IsModerator() {
		notFound(h)
		return
	}

	var titles []string
	ids := strings.Split(mux.Vars(h.r)["ids"], "/")
	for _, id := range ids {
		if id == "" {
			continue
		}
		book, err := h.db.GetBookID(id)
		if err != nil {
			h.sess.Notify("Book not found!", "The book with id '"+id+"' is not there", "error")
			continue
		}
		if err != nil {
			h.sess.Notify("An error ocurred!", err.Error(), "error")
			log.Error("Error getting book for storing '", book.Title, "': ", err.Error())
			continue
		}
		err = h.db.ActiveBook(id)
		if err != nil {
			h.sess.Notify("An error ocurred!", err.Error(), "error")
			log.Error("Error storing book '", book.Title, "': ", err.Error())
			continue
		}
		titles = append(titles, book.Title)
	}
	if titles != nil {
		h.sess.Notify("Store books!", "The books '"+strings.Join(titles, ", ")+"' are stored for public download", "success")
	}
	h.sess.Save(h.w, h.r)
	http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
}
